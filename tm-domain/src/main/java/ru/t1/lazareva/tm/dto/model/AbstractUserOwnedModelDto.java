package ru.t1.lazareva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelDto extends AbstractModelDto {

    @Nullable
    @Column(nullable = false, name = "user_id")
    private String userId;

}