package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.model.User;

import java.util.Comparator;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @NotNull
    String getSortColumnName(@NotNull final Comparator comparator);

    @NotNull Class<User> getClazz();

    @NotNull
    User create(@NotNull final String login, @NotNull final String password);

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    );

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    );

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    Boolean isLoginExists(@NotNull final String login);

    Boolean isEmailExists(@NotNull final String email);

}