package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.Project;

import java.util.Comparator;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    Project create(
            @NotNull final String userId,
            @NotNull final String name
    );

    @NotNull
    Class<Project> getClazz();

    @NotNull
    String getSortColumnName(@NotNull Comparator comparator);

}
