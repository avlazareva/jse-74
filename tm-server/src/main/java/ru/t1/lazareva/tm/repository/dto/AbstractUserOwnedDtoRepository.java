package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M> {

    @Nullable
    List<M> findAll(@NotNull final String userId);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator);

    @Nullable
    M findOneById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<M> findByUserId(@NotNull String userId);

    @Nullable
    M findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    M findByUserIdAndIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M findOneByUserIdAndIndex(@NotNull final String userId, @NotNull final Integer index);

    int getSizeByUserId(@NotNull final String userId);

    void removeByUserId(@NotNull final String userId, @NotNull final M model);

    void removeByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void removeByUserIdAndIndex(@NotNull final String userId, @NotNull final Integer index);

    void deleteByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndIndex(@NotNull String userId, @NotNull Integer index);

    M updateByUserId(@NotNull final String userId, @NotNull final M model);

    long countByUserId(@NotNull String userId);

}

