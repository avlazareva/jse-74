package ru.t1.lazareva.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.model.SessionDto;

@Repository
@Scope("prototype")
public interface SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> {

}
