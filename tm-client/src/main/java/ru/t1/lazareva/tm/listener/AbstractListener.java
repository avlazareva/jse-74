package ru.t1.lazareva.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.api.service.ITokenService;
import ru.t1.lazareva.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getName();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String description = getDescription();
        String result = "";

        final boolean hasName = name != null && !name.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();

        if (hasName) result += name;
        if (hasDescription) result += ": " + description;
        return result;
    }

}