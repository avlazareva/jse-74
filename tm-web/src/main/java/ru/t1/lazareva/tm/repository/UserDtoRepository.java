package ru.t1.lazareva.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.UserDTO;

@Repository
public interface UserDtoRepository extends JpaRepository<UserDTO, String> {

    UserDTO findByLogin(final String login);

}