package ru.t1.lazareva.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.lazareva.tm.endpoint.*;

@EnableWs
@Configuration
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/ws/*");
    }

    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(@NotNull final XsdSchema projectEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "ProjectsEndpoint")
    public DefaultWsdl11Definition projectsWsdl11Definition(@NotNull final XsdSchema projectsEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectsSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectsSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectsSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectsEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(@NotNull final XsdSchema taskEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TasksEndpoint")
    public DefaultWsdl11Definition tasksWsdl11Definition(@NotNull final XsdSchema tasksEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TasksSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TasksSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TasksSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(tasksEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "AuthEndpoint")
    public DefaultWsdl11Definition authWsdl11Definition(@NotNull final XsdSchema authEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(AuthSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(AuthSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(AuthSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(authEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean
    public XsdSchema projectsEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectsEndpoint.xsd"));
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean
    public XsdSchema tasksEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/tasksEndpoint.xsd"));
    }

    @Bean
    public XsdSchema authEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/authEndpoint.xsd"));
    }

}
