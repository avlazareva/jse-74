package ru.t1.lazareva.tm.api;

import ru.t1.lazareva.tm.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService {

    void save(final String userId, final ProjectDTO project);

    void saveAll(final String userId, final Collection<ProjectDTO> projects);

    void removeAll(final String userId);

    void removeOneById(final String userId, final String id);

    List<ProjectDTO> findAll(final String userId);

    ProjectDTO findOneById(final String userId, final String id);

}