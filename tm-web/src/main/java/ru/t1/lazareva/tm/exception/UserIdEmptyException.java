package ru.t1.lazareva.tm.exception;

public final class UserIdEmptyException extends AbstractException {

    public UserIdEmptyException() {
        super("Error! UserId is empty...");
    }

}